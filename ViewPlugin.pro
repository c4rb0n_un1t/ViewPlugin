
TARGET = ViewPlugin
TEMPLATE = lib
QT += core
QT += quickwidgets qml quick

DEFINES += QML_UIElement

include(../../plugin.pri)

include(../../Interfaces/Architecture/PluginBase/PluginBase.pri)

include(../../Interfaces/Architecture/GUIElementBase/guielementbase.pri)

HEADERS += \
	plugin.h

SOURCES += \
	plugin.cpp

DISTFILES += PluginMeta.json

RESOURCES += res.qrc
