#include "plugin.h"

Plugin::Plugin() :
	QObject(nullptr),
	PluginBase(this)
	, m_guiElementBase(new GUIElementBase(this, {"MainMenuItem"}, "qrc:/form.qml"))
{
	initPluginBase(
	{
		{INTERFACE(IPlugin), this}
		, {INTERFACE(IGUIElement), m_guiElementBase}
	},
	{
		{INTERFACE(ILogicPlugin), m_logicPlugin}
	}
	);

	m_guiElementBase->initGUIElementBase(
	{{"logicPlugin", m_logicPlugin.data()}}
	);
}

Plugin::~Plugin()
{
}
